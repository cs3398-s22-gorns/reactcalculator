import React from 'react';
import operate from "../logic/operate";
import Big from "big.js";



describe('Operate Multiplication Tests', ()=>{
    test('4 x 3 = 12 test', () => {
        expect(operate("4","3","x")).toBe("12"); 
    });
    test('16.2 x 6 = 97.2 test', () => {
        expect(operate("16.2","6","x")).toBe("97.2"); 
    });
    test('3 x 1.4 = 4.199999999999999 test', () => {
        expect(operate("3","1.4","x")).toBe("4.2"); 
    });
    
});


describe('Operate Division Tests', ()=>{
    test('4/3 = 1.333 test', () => {
        expect(parseFloat(operate("4","3","÷"))).toBeCloseTo(1.3333333333, 5); 
        // parseFloat() is a string-to-float function in Javascript.   
        // Try different precisions and string lengths to see what the test does.
    });
    test('20/5 = 4 test', () => {
        expect(operate("20","5","÷")).toBe("4"); 
    });
        test('7/0 = 0 test', () => {
        expect(operate("7","0","÷")).toBe("0"); 
    });
    
});


